package com.selftest.fragrdmi.di;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import com.selftest.fragrdmi.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

@Module
public class AppModule {
    private App app;

    public AppModule(@NonNull App app) {
        this.app  = app;
    }

    @Provides
    @Singleton
    Application provideApplication(){
        return app;
    }
}
