package com.selftest.fragrdmi.di;

import android.app.Application;

import com.selftest.fragrdmi.domain.api.NetworkModule;
import com.selftest.fragrdmi.presentation.main.MainActivity;
import com.selftest.fragrdmi.presentation.main.di.MainActivityComponent;
import com.selftest.fragrdmi.presentation.main.di.MainActivityModule;
import com.selftest.fragrdmi.presentation.sign.SignActivity;
import com.selftest.fragrdmi.presentation.sign.di.SignActivityComponent;
import com.selftest.fragrdmi.presentation.sign.di.SignActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

@Component(modules = {AppModule.class, NetworkModule.class})
@Singleton
public interface AppComponent {
    SignActivityComponent plus(SignActivityModule module);
    MainActivityComponent plus(MainActivityModule module);
}
