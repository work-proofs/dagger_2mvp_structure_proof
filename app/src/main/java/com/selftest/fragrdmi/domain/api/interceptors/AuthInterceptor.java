package com.selftest.fragrdmi.domain.api.interceptors;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by andrii.puhach on 09.02.2017.
 */

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        // auth somehow
        return chain.proceed(chain.request());
    }
}
