package com.selftest.fragrdmi.domain.database;

import com.selftest.fragrdmi.data.entities.BaseEntity;

import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

/**
 * Created by andrii.puhach on 07.02.2017.
 */

@Singleton
public class DatabaseManager implements IDatabaseManager {
    @Override
    public List<BaseEntity> entities() {
        return Collections.emptyList();
    }
}
