package com.selftest.fragrdmi.domain.interactors;

import com.selftest.fragrdmi.domain.api.IOptilogApi;
import com.selftest.fragrdmi.data.entities.User;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by andrii.puhach on 07.02.2017.
 */

@Singleton
public class AuthInteractor {

    IOptilogApi api;

    @Inject
    public AuthInteractor(IOptilogApi api) {
        this.api = api;
    }

    public interface ISign{
        void onSuccess(User user);
        void onCompleted();
    }

    public void signIn(String login,String password,final ISign sign){
        api.signIn(login,password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                sign.onSuccess(response.body());
                sign.onCompleted();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                //use general error handler
                sign.onCompleted();
            }
        });
    }
}
