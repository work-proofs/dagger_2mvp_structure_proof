package com.selftest.fragrdmi.domain.database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andrii.puhach on 07.02.2017.
 * use realm or sqlitehelper
 */

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    DatabaseManager provideDatabaseManager(){
        return new DatabaseManager();
    }
}
