package com.selftest.fragrdmi.domain.database;

import com.selftest.fragrdmi.data.entities.BaseEntity;

import java.util.List;

/**
 * Created by andrii.puhach on 07.02.2017.
 */

public interface IDatabaseManager {
    List<BaseEntity> entities();
}
