package com.selftest.fragrdmi.domain.api;

import com.selftest.fragrdmi.data.entities.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

public interface IOptilogApi {
    @FormUrlEncoded
    @POST("/v1/sign_in")
    Call<User> signIn(@Field("login") String login, @Field("password") String password);
}
