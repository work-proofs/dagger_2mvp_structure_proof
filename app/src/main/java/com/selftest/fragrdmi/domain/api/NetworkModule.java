package com.selftest.fragrdmi.domain.api;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.selftest.fragrdmi.Constants;
import com.selftest.fragrdmi.R;
import com.selftest.fragrdmi.domain.api.interceptors.AuthInterceptor;
import com.selftest.fragrdmi.domain.interactors.AuthInteractor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(AuthInterceptor authInterceptor){
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(authInterceptor)
                .readTimeout(60,TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Application app,  OkHttpClient client){
        final SharedPreferences sharedPreferences = app.getSharedPreferences(Constants.SHARED_PREFS_KEY, Context.MODE_PRIVATE);
        final String baseUrl = sharedPreferences.getString(Constants.BASE_URL,app.getString(R.string.base_url));
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }

    @Provides
    @Singleton
    IOptilogApi provideApi(Retrofit retrofit) {
        return retrofit.create(IOptilogApi.class);
    }

    @Provides
    @Singleton
    AuthInterceptor provideAuthInterceptor(){
        return new AuthInterceptor();
    }
}
