package com.selftest.fragrdmi.utils.interfaces;

public interface BiConsumer<T1, T2> {
    void accept(T1 value1, T2 value2);
}
