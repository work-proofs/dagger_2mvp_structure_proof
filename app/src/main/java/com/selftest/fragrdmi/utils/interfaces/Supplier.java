package com.selftest.fragrdmi.utils.interfaces;

public interface Supplier<T> {
    T supply();
}
