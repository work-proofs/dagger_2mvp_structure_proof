package com.selftest.fragrdmi.utils.interfaces;

public interface Predicate<T>{
    boolean select(T item);
}
