package com.selftest.fragrdmi.utils.interfaces;

public interface Func1<T, TResult> {
    TResult func(T arg);
}
