package com.selftest.fragrdmi.utils;

import android.view.View;

/**
 * Created by andrii.puhach on 09.02.2017.
 */

public class ViewUtils {
    public static  <T> T findParentOfType(View v, Class<T> pClazz){
        if(v.getClass().getCanonicalName().equals(pClazz.getCanonicalName())){
            return (T) v;
        }
        return findParentOfType((View) v.getParent(),pClazz);
    }
}
