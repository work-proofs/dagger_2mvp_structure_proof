package com.selftest.fragrdmi.utils.interfaces;

//naming q. consumer/biconsumer
public interface Consumer<T> {
    void accept(T value);
}
