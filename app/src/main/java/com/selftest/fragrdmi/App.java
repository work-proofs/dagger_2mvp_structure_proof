package com.selftest.fragrdmi;

import android.app.Application;
import android.content.Context;

import com.selftest.fragrdmi.di.AppComponent;
import com.selftest.fragrdmi.di.AppModule;
import com.selftest.fragrdmi.di.DaggerAppComponent;
import com.selftest.fragrdmi.domain.api.NetworkModule;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

public class App extends Application {
    private AppComponent component;

    private AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = createComponent();
    }

    public AppComponent getAppComponent() {
        return component;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }
}
