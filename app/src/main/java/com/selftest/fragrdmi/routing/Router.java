package com.selftest.fragrdmi.routing;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by andrii.puhach on 08.02.2017.
 */


//write same for fragments
public class Router {

    public static void navigate(Route route) {
        final Intent intent = route.toIntent();
        route.getSource().startActivity(intent);
    }

    public static void navigateWithCode(Route route, int requestCode) {
        final Intent intent = route.toIntent();
        route.getSource().startActivityForResult(intent, requestCode);
    }
}
