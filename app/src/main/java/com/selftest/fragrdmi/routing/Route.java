package com.selftest.fragrdmi.routing;

import android.app.Activity;
import android.content.Intent;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by andrii.puhach on 08.02.2017.
 */

public class Route {
    private Activity source;
    private Class<? extends Activity> destination;
    private Map<String, Object> params;

    private Route(Activity source, Class<? extends Activity> destination, Map<String, Object> params) {
        this.source = source;
        this.destination = destination;
        this.params = params;
    }

    public static Builder builder(Activity from) {
        return new Builder(from);
    }

    Intent toIntent() {
        final Intent intent = new Intent(getSource(), getDestination());
        for (Map.Entry<String, Object> entry : getParams().entrySet()) {
            intent.putExtra(entry.getKey(), (Serializable) entry.getValue());
        }
        return intent;
    }

    public static class Builder {
        Activity from;
        Class<? extends Activity> to;
        Map<String, Object> params;

        private Builder(Activity from) {
            this.from = from;
            this.params = new HashMap<>();
        }

        public Builder to(Class<? extends Activity> clazz) {
            this.to = clazz;
            return this;
        }

        public Builder param(String key, Object value) {
            params.put(key, value);
            return this;
        }

        public Route build() {
            // throw exception if no params or destination or source activity
            return new Route(from, to, params);
        }
    }

    public Activity getSource() {
        return source;
    }

    public Class<? extends Activity> getDestination() {
        return destination;
    }

    public Map<String, Object> getParams() {
        return params;
    }
}
