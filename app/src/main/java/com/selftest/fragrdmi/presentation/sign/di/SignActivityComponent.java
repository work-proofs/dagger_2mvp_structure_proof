package com.selftest.fragrdmi.presentation.sign.di;

import com.selftest.fragrdmi.presentation.sign.SignActivity;
import com.selftest.fragrdmi.di.PerActivity;

import dagger.Subcomponent;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

@Subcomponent(modules = {SignActivityModule.class})
@PerActivity
public interface SignActivityComponent {
    void inject(SignActivity signActivity);
}
