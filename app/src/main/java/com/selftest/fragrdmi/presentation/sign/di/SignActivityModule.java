package com.selftest.fragrdmi.presentation.sign.di;

import com.selftest.fragrdmi.domain.api.IOptilogApi;
import com.selftest.fragrdmi.domain.interactors.AuthInteractor;
import com.selftest.fragrdmi.presentation.sign.SignActivityPresenter;
import com.selftest.fragrdmi.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

@Module
public class SignActivityModule {

    @Provides
    @PerActivity
    SignActivityPresenter providePresenter(AuthInteractor interactor){
        return new SignActivityPresenter(interactor);
    }

    @Provides
    @PerActivity
    AuthInteractor provideAuthInteractor(IOptilogApi api){
        return new AuthInteractor(api);
    }
}
