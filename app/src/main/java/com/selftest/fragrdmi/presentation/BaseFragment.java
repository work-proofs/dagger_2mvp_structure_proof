package com.selftest.fragrdmi.presentation;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.selftest.fragrdmi.presentation.BasePresenter;

import java.util.Map;

import javax.inject.Inject;

/**
 * Created by andrii.puhach on 08.02.2017.
 */

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements BasePresenter.Contract{
    @Inject
    T presenter;

    public T getPresenter(){
        return presenter;
    }

    public void setupContract(){
        if(presenter!=null) {
            presenter.setContract(this);
        }
    }
}
