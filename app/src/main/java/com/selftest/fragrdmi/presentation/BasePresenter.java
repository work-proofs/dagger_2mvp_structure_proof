package com.selftest.fragrdmi.presentation;

/**
 * Created by andrii.puhach on 07.02.2017.
 */

public abstract class BasePresenter<T extends BasePresenter.Contract> {
    public interface Contract{
    }

    private T contract;

    public void setContract(T contract) {
        this.contract = contract;
    }

    public T getViewContract(){
        return contract;
    }
}
