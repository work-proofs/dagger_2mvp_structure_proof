package com.selftest.fragrdmi.presentation.move;

import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.selftest.fragrdmi.R;
import com.selftest.fragrdmi.presentation.BaseFragment;
import com.selftest.fragrdmi.presentation.custom.CustomDashedAutocompleteEditText;
import com.selftest.fragrdmi.presentation.custom.CustomDashedInputLayout;
import com.selftest.fragrdmi.presentation.main.MainActivity;
import com.selftest.fragrdmi.presentation.move.di.MoveFragmentComponent;
import com.selftest.fragrdmi.presentation.move.di.MoveFragmentModule;

import java.util.Map;

/**
 * Created by andrii.puhach on 08.02.2017.
 */

public class MoveFragment extends BaseFragment<MoveFragmentPresenter> implements MoveFragmentPresenter.Contract, View.OnFocusChangeListener {

    private MoveFragmentComponent moveFragmentComponent;

    private CustomDashedInputLayout customerInputLayout;
    private CustomDashedInputLayout skuInputLayout;
    private CustomDashedInputLayout moveFromInputLayout;
    private CustomDashedInputLayout currentPalletInputLayout;

    public MoveFragment() {
        this.setRetainInstance(true);
    }

    public static MoveFragment newInstance(@Nullable Map<String, Object> params) {
        return new MoveFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDagger();
    }

    private void initViews(View root){
        customerInputLayout = (CustomDashedInputLayout) root.findViewById(R.id.customerAutocompleteEditTextLayout);
        skuInputLayout = (CustomDashedInputLayout) root.findViewById(R.id.skuAutocompleteEditTextLayout);
        moveFromInputLayout = (CustomDashedInputLayout) root.findViewById(R.id.moveFromAutocompleteEditTextLayout);
        currentPalletInputLayout = (CustomDashedInputLayout) root.findViewById(R.id.currentPalletAutocompleteEditTextLayout);

        customerInputLayout.getEditText().setOnFocusChangeListener(this);
        skuInputLayout.getEditText().setOnFocusChangeListener(this);
        moveFromInputLayout.getEditText().setOnFocusChangeListener(this);
        currentPalletInputLayout.getEditText().setOnFocusChangeListener(this);
    }

    private void setupDagger() {
        if(getActivity() == null){
            return;
        }
        moveFragmentComponent = ((MainActivity) getActivity()).getMainActivityComponent().plus(new MoveFragmentModule());
        moveFragmentComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.move_fragment, container, false);
        setupContract();
        initViews(v);
        return v;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        CustomDashedAutocompleteEditText textView = (CustomDashedAutocompleteEditText) v;
        if(hasFocus) return;
        //move validation logic to presenter
        //something like
        //switch(v){ case sku: presenter.validateSku(textView.getText().toString()))}
        // where validateSku will return boolean if view is valid so it can be further processed (seems legit)
        // or move the after validation message show to view contract (seems nice, but presenter will have to somehow distinguish the view , and as passing view is not allowed
        // it will have to depend on some enum ValidatingViews
        if(TextUtils.isEmpty(textView.getText().toString())){
            textView.getInputParent().setError("Must not be empty");
        }
        else{
            textView.getInputParent().setError(null);
        }
    }
}
