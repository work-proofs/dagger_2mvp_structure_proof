package com.selftest.fragrdmi.presentation.main.di;

import com.selftest.fragrdmi.di.PerActivity;
import com.selftest.fragrdmi.presentation.main.MainActivityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andrii.puhach on 08.02.2017.
 */

@Module
public class MainActivityModule {
    @Provides
    @PerActivity
    public MainActivityPresenter provideMainActivityPresenter(){
        return new MainActivityPresenter();
    }
}
