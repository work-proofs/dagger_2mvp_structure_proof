package com.selftest.fragrdmi.presentation;

import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

/**
 * Created by andrii.puhach on 07.02.2017.
 */

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements BasePresenter.Contract{

    @Inject T presenter;

    public T getPresenter(){
        return presenter;
    }

    public void setupContract(){
        if(presenter!=null) {
            presenter.setContract(this);
        }
    }
}
