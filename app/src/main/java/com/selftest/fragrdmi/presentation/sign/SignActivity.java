package com.selftest.fragrdmi.presentation.sign;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.selftest.fragrdmi.App;
import com.selftest.fragrdmi.R;
import com.selftest.fragrdmi.data.entities.User;
import com.selftest.fragrdmi.presentation.BaseActivity;
import com.selftest.fragrdmi.presentation.main.MainActivity;
import com.selftest.fragrdmi.presentation.sign.di.SignActivityModule;
import com.selftest.fragrdmi.routing.RequestCodes;
import com.selftest.fragrdmi.routing.Route;
import com.selftest.fragrdmi.routing.Router;

public class SignActivity extends BaseActivity<SignActivityPresenter> implements SignActivityPresenter.Contract, View.OnClickListener {

    private EditText passwordEditText;
    private EditText loginEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDagger();
        setContentView(R.layout.sign_activity);
        setupContract();
        initViews();
    }

    private void setupDagger() {
        App.get(this).getAppComponent().plus(new SignActivityModule()).inject(this);
    }

    private void initViews() {
        View button = findViewById(R.id.signInButton);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signInButton:
                final String login = getLogin();
                final String password = getPassword();
                //validate somehow
                getPresenter().signIn(login, password);
                break;
            default:
                break;
        }
    }

    @Override
    public String getLogin() {
        if (loginEditText == null) {
            loginEditText = (EditText) findViewById(R.id.loginEditText);
        }
        return loginEditText.getText().toString();
    }

    @Override
    public String getPassword() {
        if (passwordEditText == null) {
            passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        }
        return passwordEditText.getText().toString();
    }

    @Override
    public void onSuccssfulSignUp(User user) {
        // show alert hello $user_name perhapse
        Log.i(SignActivity.class.getCanonicalName(), "Hello!");
        Router.navigateWithCode(
                Route.builder(this)
                        .param("hello", "world")
                        .param("world", "hello")
                        .to(MainActivity.class)
                        .build(),
                RequestCodes.SIGN_TO_MAIN
        );
    }

}
