package com.selftest.fragrdmi.presentation.main;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.selftest.fragrdmi.App;
import com.selftest.fragrdmi.R;
import com.selftest.fragrdmi.presentation.BaseActivity;
import com.selftest.fragrdmi.presentation.BaseFragment;
import com.selftest.fragrdmi.presentation.main.di.MainActivityComponent;
import com.selftest.fragrdmi.presentation.main.di.MainActivityModule;
import com.selftest.fragrdmi.presentation.move.MoveFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by andrii.puhach on 08.02.2017.
 */

public class MainActivity extends BaseActivity<MainActivityPresenter> implements MainActivityPresenter.Contract, DrawerLayout.DrawerListener, ListView.OnItemClickListener {

    private static final String STATE_SELECTED_POSITION = "state_selected_position";

    private String[] drawerItems = new String[]{
            "Move 1",
            "Move 2"
    };

    private ListView drawerItemsListView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;

    private BaseFragment currentFragment;

    private MainActivityComponent mainActivityComponent;
    private Toolbar toolbar;


    private void setupDagger() {
        mainActivityComponent = App.get(this).getAppComponent().plus(new MainActivityModule());
        mainActivityComponent.inject(this);
    }

    public MainActivityComponent getMainActivityComponent(){
        return mainActivityComponent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDagger();
        setContentView(R.layout.main_activity);
        setupContract();
        initViews();

        boolean instanceSaved = savedInstanceState != null;
        getPresenter().selectItem(instanceSaved ? savedInstanceState.getInt(STATE_SELECTED_POSITION) : 0);
    }

    private void initViews() {
        //todo: move to something like toolbar activity perhapse
        initToolbar();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer_content_description, R.string.close_drawer_content_description);
        drawerToggle.setDrawerIndicatorEnabled(true);

        drawerItemsListView = (ListView) findViewById(R.id.drawer_list);

        drawerLayout.addDrawerListener(this);
        drawerLayout.addDrawerListener(drawerToggle);

        drawerItemsListView.setOnItemClickListener(this);
        drawerItemsListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_widget);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(drawerToggle.isDrawerIndicatorEnabled()){
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar!=null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getPresenter().selectItem(position);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, getPresenter().getSelectedItemPosition());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContentUpdate(int selectedFragmentPos) {
        Map<String,Object> params = new HashMap<>();

        switch (selectedFragmentPos) {
            //move to constants
            case DrawerItemsConstants.FIRST_OPTION:
                params.put("text","1");
                currentFragment = MoveFragment.newInstance(params);
                break;
            case DrawerItemsConstants.SECOND_OPTION:
                params.put("text","2");
                currentFragment = MoveFragment.newInstance(params);
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, currentFragment).commitAllowingStateLoss();
        drawerLayout.closeDrawer(Gravity.LEFT);
    }
}
