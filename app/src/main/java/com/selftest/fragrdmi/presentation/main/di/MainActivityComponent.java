package com.selftest.fragrdmi.presentation.main.di;

import com.selftest.fragrdmi.di.PerActivity;
import com.selftest.fragrdmi.presentation.main.MainActivity;
import com.selftest.fragrdmi.presentation.move.MoveFragment;
import com.selftest.fragrdmi.presentation.move.di.MoveFragmentComponent;
import com.selftest.fragrdmi.presentation.move.di.MoveFragmentModule;

import dagger.Subcomponent;

/**
 * Created by andrii.puhach on 08.02.2017.
 */

@Subcomponent(modules = MainActivityModule.class)
@PerActivity
public interface MainActivityComponent {
    void inject(MainActivity activity);
    // a lot of fragments
    MoveFragmentComponent plus(MoveFragmentModule moveFragmentComponent);
}
