package com.selftest.fragrdmi.presentation.sign;

import com.selftest.fragrdmi.data.entities.User;
import com.selftest.fragrdmi.domain.interactors.AuthInteractor;
import com.selftest.fragrdmi.presentation.BasePresenter;

import javax.inject.Inject;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

public class SignActivityPresenter extends BasePresenter<SignActivityPresenter.Contract> {

    private AuthInteractor interactor;

    interface Contract extends BasePresenter.Contract{
        String getLogin();
        String getPassword();

        void onSuccssfulSignUp(User user);
    }

    void signIn(String login, String password) {
        interactor.signIn(login, password, new AuthInteractor.ISign() {
            @Override
            public void onSuccess(User user) {

            }

            @Override
            public void onCompleted() {
                //show something
                getViewContract().onSuccssfulSignUp(null);
            }

        });
    }

    @Inject
    public SignActivityPresenter(AuthInteractor interactor) {
        this.interactor = interactor;
    }
}
