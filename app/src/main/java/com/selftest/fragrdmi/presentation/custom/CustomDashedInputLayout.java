package com.selftest.fragrdmi.presentation.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.widget.EditText;

import com.selftest.fragrdmi.R;

/**
 * Created by andrii.puhach on 09.02.2017.
 */

public class CustomDashedInputLayout extends TextInputLayout {
    public CustomDashedInputLayout(Context context) {
        super(context);
    }

    public CustomDashedInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomDashedInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setError(@Nullable CharSequence error) {
        super.setError(error);
        triggerBackgroundRedraw();
    }

    private void triggerBackgroundRedraw() {
        CustomDashedAutocompleteEditText editText = (CustomDashedAutocompleteEditText) getEditText();
        editText.setBackgroundDrawable(editText.getBackground());
    }
}
