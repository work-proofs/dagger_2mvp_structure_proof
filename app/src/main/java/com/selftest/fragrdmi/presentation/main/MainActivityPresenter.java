package com.selftest.fragrdmi.presentation.main;

import com.selftest.fragrdmi.presentation.BasePresenter;

/**
 * Created by andrii.puhach on 08.02.2017.
 */

public class MainActivityPresenter extends BasePresenter<MainActivityPresenter.Contract> {
  
    private int selectedItemPosition = 0;

    public int getSelectedItemPosition() {
        return selectedItemPosition;
    }

    interface Contract extends BasePresenter.Contract{
        void onContentUpdate(int selectedFragmentPos);
    }

    void selectItem(int pos){
        selectedItemPosition = pos;
        getViewContract().onContentUpdate(selectedItemPosition);
    }
}
