package com.selftest.fragrdmi.presentation.move.di;

import com.selftest.fragrdmi.di.PerActivity;
import com.selftest.fragrdmi.di.PerFragment;
import com.selftest.fragrdmi.presentation.move.MoveFragment;
import com.selftest.fragrdmi.presentation.sign.SignActivity;

import dagger.Subcomponent;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

@Subcomponent(modules = {MoveFragmentModule.class})
@PerFragment
public interface MoveFragmentComponent {
    void inject(MoveFragment moveFragment);
}
