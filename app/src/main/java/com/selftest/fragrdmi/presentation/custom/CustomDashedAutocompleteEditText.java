package com.selftest.fragrdmi.presentation.custom;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

import com.selftest.fragrdmi.R;
import com.selftest.fragrdmi.utils.ViewUtils;

/**
 * Created by andrii.puhach on 09.02.2017.
 */

public class CustomDashedAutocompleteEditText extends AppCompatAutoCompleteTextView {

    private CustomDashedInputLayout parentInputLayout;

    public CustomDashedAutocompleteEditText(Context context) {
        super(context);
    }

    public CustomDashedAutocompleteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomDashedAutocompleteEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * duct tape because of text input layout empty field issue
     *
     * @return
     */

    public CustomDashedInputLayout getInputParent() {
        if (parentInputLayout == null) {
            parentInputLayout = ViewUtils.findParentOfType(this, CustomDashedInputLayout.class);
        }
        return parentInputLayout;
    }

    @Override
    public Drawable getBackground() {
        if (getInputParent().getError() != null) {
            return ContextCompat.getDrawable(getContext(), R.drawable.edit_text_background_error);
        }
        return ContextCompat.getDrawable(getContext(), R.drawable.edit_text_background_state);
    }
}
