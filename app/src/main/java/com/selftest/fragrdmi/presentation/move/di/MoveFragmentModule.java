package com.selftest.fragrdmi.presentation.move.di;

import com.selftest.fragrdmi.di.PerFragment;
import com.selftest.fragrdmi.presentation.move.MoveFragmentPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andrii.puhach on 06.02.2017.
 */

@Module
public class MoveFragmentModule {

    @Provides
    @PerFragment
    MoveFragmentPresenter providePresenter(){
        return new MoveFragmentPresenter();
    }
}
