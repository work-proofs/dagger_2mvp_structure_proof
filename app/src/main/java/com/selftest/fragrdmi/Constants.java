package com.selftest.fragrdmi;

/**
 * Created by andrii.puhach on 09.02.2017.
 */
public class Constants {
    public final static String SHARED_PREFS_KEY = "default_shared_prefs";
    public static final String BASE_URL = "base_url";
}
